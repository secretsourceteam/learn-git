# About this repository

The primary aim of this repo is to give people an opportunity to practice using git on a 
team project, working with feature branches, tagging, resolving conflicts, and creating, 
reviewing, merging and closing PRs (Pull Requests).

This tutorial assumes you are on a unix-style computer and using a bash-compatible
terminal. If you are not, you will need to substitute some of the non-git commands for
alternatives.

## Note for Teachers

If you are teaching git to others, please see the "rubric" branch for details on the expected outcomes from each part of this tutorial. Our intention is that "correcting" a student's work can be mostly or completely automated.

## Alternative tutorials and workshops

- [Basic git practice](https://github.com/imintel/practice-git)
- [Learn about branching in git](https://learngitbranching.js.org/)
- [The GitGame](https://github.com/git-game/git-game), a place to test your knowledge of 
git once you've gone through some tutorials and gotten some practice.

If you get really stuck, you might find [ohshitgit](https://ohshitgit.com/) to be a 
helpful resource for undoing whatever you did accidentally.

## What is Git?

Git is a *distributed* **version control** system. It allows two or more people to change
the same file without overwriting each other's changes. Such a system is essential when
working on a project as a team. Git offers many, many more advantages but we'll cover
those elsewhere.

## Getting Git

On Mac and Linux git (normally) comes pre-installed. You can test by opening a command
line and typing `git version`. If you see anything other than 
`git version 2.32.0 (Apple Git-132)` or similar, then git might not be installed yet.

You can install git [from here](https://github.com/git-guides/install-git).

## Setup

First thing to do is to setup your identity. This identifies you to
other people who download the project.

    $ git config --global user.name "Your Name"
    $ git config --global user.email your.email@example.com

As a helpful step, you may want to set Git to use your favourite editor

    $ git config --global core.editor emacs

## Starting your journey

First, fork and then clone this repository. The method for forking a repository depends on
the repository hosting platform. For Github, [follow these instructions](https://docs.github.com/en/get-started/quickstart/fork-a-repo).
For Bitbucket, [follow these instructions](https://support.atlassian.com/bitbucket-cloud/docs/fork-a-repository/).

### Secret Source Technology SL Specific instructions

For Bitbucket (at Secret Source), you must specify the following:

- Workspace: secretsource
- Project: Internship
- Name: learn-git-(your name)

### Resume…

Once you've forked the repository, you'll need to clone it to start this tutorial.

(Substitute "(your name)" for your first name plus any unique identifier if your name is
already in use by another repo - no spaces in the name please)

    $ git clone git@bitbucket.org:secretsourceteam/learn-git-(your name).git

Once you have cloned your repository, you should now see a directory
called `learn-git-(your name)`. This is your __working directory__

    $ cd learn-git-(your name)
    $ ls

For the curious, you should also see the `.git` subdirectory. This is
where all your repository’s data and history is kept.

    $ ls -a .git

You will see:

    branches  config  description  HEAD  hooks  info  objects  refs

## The Staging Area

Now, let’s try adding some files into the project. Create a couple of
files.

Let’s create two files named `bob.txt` and `alice.txt`.

    $ touch alice.txt bob.txt

Let’s use a mail analogy.

In Git, you first add content to the `staging area` by using `git add`.
This is like putting the stuff you want to send into a cardboard box.
You finalize the process and record it into the git index by using
`git commit`. This is like sealing the box - it’s now ready to send.

Let’s add the files to the staging area

    $ git add alice.txt bob.txt

## Committing

You are now ready to commit. The `-m` flag allows you to enter a message
to go with the commit at the same time.

    $ git commit -m "I am adding two new files"

## Let’s see what just happened

We should now have a new commit. To see all the commits so far, use
`git log`

    $ git log

The log should show all commits listed from most recent first to least
recent. You would see various information like the name of the author,
the date it was commited, a commit SHA number, and the message for the
commit.

You should also see your most recent commit, where you added the two new
files in the previous section. However git log does not show the files
involved in each commit. To view more information about a commit, use
`git show`.

    $ git show

You should see something similar to:

    commit 5a1fad96c8584b2c194c229de7e112e4c84e5089
    Author: kuahyeow 
    Date:   Sun Jul 17 19:13:42 2011 +1200

        I am adding two new files

    diff --git a/alice.txt b/alice.txt
    new file mode 100644
    index 0000000..e69de29
    diff --git a/bob.txt b/bob.txt
    new file mode 100644
    index 0000000..e69de29

## A Necessary Digression

In this section, we are going to add more changes, and try to recover
from mistakes.

Be forewarned, this next step is going to be hard. We will need to add
some content to alice.txt.

Open `alice.txt` and type in your favourite line from a song, or:

e.g. Lorem ipsum Sed ut perspiciatis, unde omnis iste natus error sit
voluptatem accusantium doloremque laudantium

Then **save** the file

What did we change? A very useful command is `git diff`. This is very
useful to see exactly what changes you have done.

    $ git diff

You should see something like the following:

    diff --git a/alice.txt b/alice.txt
    index e69de29..2aedcab 100644
    --- a/alice.txt
    +++ b/alice.txt
    @@ -0,0 +1 @@
    +Lorem ipsum Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium

Understanding this output is left as an exercise for the reader. There are many places on
the interwebs where this output is explained.

## Staging area again

Now let’s add our modified file, `alice.txt` to the staging area. Do you
remember how ?

Next, check the `status` of `alice.txt`. Is it in the staging area now?

## Undoing

Let’s say we did not like putting Lorem ipsum into `alice.txt`. One
advantage of a staging area is to enable us to back out before we
commit - which is a bit harder to back out of. Remembering the mail
analogy - it’s easier to take mail out of the cardboard box before you
seal it than after.

Here’s how to back out of the staging area:

    $ git reset HEAD alice.txt

Compare the `git status` now to the git status from the previous
section. How does it differ?

Your staging area should now be empty. What’s happened to the Lorem
Ipsum changes? It’s still there. We are now back to the state just
before we added this file to staging area. Going back to the mail
analogy, we just took our letter out of the box.

## Undoing II

Sometimes we did not like what we have done and we wish to go back to
the last *recorded* state. In this case, we wish to go back to the state
just before we added the Lorem ipsum text to `alice.txt`.

To accomplish this, we use `git checkout`, like so:

    $ git checkout alice.txt

You have now un-done your changes. Your file is now empty.

## Branching

Most large code bases have at least two branches - a ‘live’ branch and a
‘development’ branch. The live branch is code which is OK to be deployed
on to a website, or downloaded by customers. The development branch
allows developers to work on features which might not be bug free. Only
once everyone is happy with the development branch would it be merged
with the live branch.

Creating a branch in Git is easy. The `git branch` command, when used by
itself, will list the branches you currently have

    $ git branch

The `*` should indicate the current branch you are on, which is
`main`.

If you wish to start another branch, use
`git checkout -b (new-branch-name)` :

    $ git checkout -b exp1

Try git branch again to check which branch you are currently on:

    $ git branch
    * exp1
      main

The new branch is now created and you should be working on it. If not, use the command
below to switch to the new branch:

    $ git checkout exp1

`git checkout (branch-name)` is used to switch branches.

Let’s perform some commits now,

    $ echo 'some content' > test.txt
    $ git add test.txt
    $ git commit -m "Added experimental txt"

Now, let’s compare them to the main branch. Use `git diff`

    $ git diff main

Basically what the above output says is that `test.txt` is present on
the `exp1` branch, but is absent on the `main` branch.

## Now you see me, now you don’t

Git is good enough to handle your files when you switch between
branches. Switch back to the `main` branch

Try switching back to the main branch (Hint: It’s the same command we
used to switch to the exp1 branch above)

Now, where’s our `test.txt` file ?

    $ ls
    README.md  alice.txt   bob.txt     gamow.txt

As you can see the new file you created in the other branch has
disappeared. Not to worry, it is safely tucked away, and will re-appear
when you switch back to that branch.

Now, switch back to the exp1 branch, and check that the `test.txt` is
now present.

## Merging

We now try out merging. Eventually you will want to merge two branches
together after the conclusion of work. `git merge` allows you to do that.

Git merging works by first switching the branch you want to *into*, and
then running the command to merge the other branch in.

We now want to merge our `exp1` branch into `main`. First, switch to
the `main` branch.

    git checkout main

Next, we merge the `exp1` branch into `main` :

    $ git merge exp1

Do you see the following output ?

    Merge made by recursive.
     test.txt |    1 +
     1 files changed, 1 insertions(+), 0 deletions(-)
     create mode 100644 test.txt

You have to be in the branch you want merge *into* and then you always
specify the branch you want to merge.

At this point, you can visual the changes using the following command.
We'll save an explanation of this one for later…

    $ git log --pretty=oneline --abbrev-commit --graph --decorate --all

## Sharing Your Work

Git, besides helping you manage your own work, shines most when you work
on a team. In order for others to see your changes, you need to _push_ 
them to the repository, often Github or Bitbucket. Now that you've 
merged your local branch onto the main branch, it's time to share that
work with the other members of your team. Execute the following command
to push your changes to the hosted repo so others can see them:

    $ git push -u origin main

Your changes are now on the repository for others to see.

## Merge Conflicts

Git is pretty good at merging automagically, even when the same file is
edited. There are however, some situations where the same line of code
is edited there is no way a computer can figure out how to merge. 
This will trigger a conflict which you will have to fix.

We now practise fixing merge conflicts. Recall that conflicts are caused
by merges which affect the same block of code.

Here’s a branch I prepared earlier. The branch is called `alpher`. Run
the code below to set it up (don’t worry if you can’t understand it)

    $ git checkout alpher

You should now have a new branch called `alpher`. Try merging that
branch into `main` now and fix the ensuing conflict.

## Fixing a conflict

You should see a `conflict` with the `gamow.txt` file. This means that
the same line of text was edited and committed on both the main branch
and the alpher branch. The output below basically tells you the current
situation :

    Auto-merging gamow.txt
    CONFLICT (content): Merge conflict in gamow.txt
    Automatic merge failed; fix conflicts and then commit the result.

If you open the `gamow.txt` file, you will see something similar as
below:

    $ cat gamow.txt
    <<<<<<< HEAD
    It was eventually recognized that most of the heavy elements observed in the present universe are the result of stellar nucleosynthesis (http://en.wikipedia.org/wiki/Stellar_nucleosynthesis) in stars, a theory largely developed by Bethe.
    =======

    http://en.wikipedia.org/wiki/Stellar_nucleosynthesis
    Stellar nucleosynthesis is the collective term for the nuclear reactions taking place in stars to build the nuclei of the elements heavier than hydrogen. Some small quantity of these reactions also occur on the stellar surface under various circumstances. For the creation of elements during the explosion of a star, the term supernova nucleosynthesis is used.
    >>>>>>> alpher

Git uses pretty much standard conflict resolution markers. The top part
of the block, which is everything between `<<<<<< HEAD` and `======` is
what was in your current branch.

The bottom half is the version that is present from the `alpher` branch.
To resolve the conflict, you either choose one side or merge them as you
see fit.

For example, I might decide to choose the version from the `alpher`
branch.

Now, try to **fix the merge conflict**. Pick the text that you think is
better (Ask for help if stumped)

Once I have done that, I can then mark the conflict as fixed by using
`git add` and `git commit`.

    $ git add gamow.txt
    $ git commit -m "Fixed conflict"

Congratulations. You have fixed the conflict. All is good in the world.

## Fin

You have learnt:

1.  Clone a repository
2.  Commit files
3.  Check status
4.  Check diff
5.  Undoing changes
6.  Branching and merging
7.  Fixing conflicts


Now You can choose two tracks, either Part II (below) which covers time travel and
mangling your git history, or Part III (even below-er) which covers Github pull
requests and cat gifs.

# Part II - Undoing

Check out the `revert` branch on this repository for further instructions!
You can always get back to this version of the readme by checking out the main
branch.

# Part III - The git "clipboard"

Check out the `stashaholic` branch for Part III.

## References and Further reading

I throughly recommend these resources to continue your Git practice:

-   <a href="http://try.github.com">http://try.github.com</a> Another
    beginners tutorial for git
-   <a href="http://git-scm.com">http://git-scm.com</a> Official
    website, with very useful help, book and videos
-   <a href="http://gitref.org">http://gitref.org</a>
-   <a href="http://www.kernel.org/pub/software/scm/git/docs/everyday.html">http://www.kernel.org/pub/software/scm/git/docs/everyday.html</a>

## Author

This work is licensed under the Creative Commons
Attribution-NonCommercial-ShareAlike 3.0 License\
<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/">http://creativecommons.org/licenses/by-nc-sa/3.0/</a>\
Author: Thong Kuah\
Contributors: Andy Newport, Nick Malcolm, [Ted Stresen-Reuter](https://github.com/tedsecretsource/)
